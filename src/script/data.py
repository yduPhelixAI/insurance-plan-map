import pandas as pd
import numpy as np
from sklearn.model_selection import train_test_split
import seaborn as sns
import matplotlib.pyplot as plt
from ..utils.constants import *
from ..utils.utils import log, save_img


def transfer(dictionary, arr):
    def func(_key):
        return dictionary[_key]

    return list(map(func, arr))


class Data:
    fileName = "policy-number-2021-4-12.csv"
    planName = 'insplanname'
    location = 'location_state'
    policy = 'insPolicyNumber'
    test_size = 0.2
    random_state = random_state

    def __init__(self, fileName=0):
        if fileName != 0:
            self.fileName = fileName
        print("self.fileName = ", self.fileName)
        self.data = pd.read_csv(self.fileName)
        print(self.data.head(10))

    ############################## Private ##############################

    def __clean_data(self):

        def rm_dup():
            log("Removing Duplicates")
            self.data = self.data.drop_duplicates()
            unique_counts = pd.DataFrame.from_records(
                [(col, self.data[col].nunique()) for col in self.data.columns],
                columns=['feature', 'num_unique']).sort_values(by='num_unique')

            print(unique_counts)

        def rm_na():
            log("Removing NAs")
            is_NaN = self.data.isna()
            print(is_NaN.sum(axis=0))
            self.data.dropna(inplace=True)  # for now
            self.data.dropna(inplace=True)
            self.data[self.planName] = self.data[self.planName].str.split("-").str[0]
            unique_counts = pd.DataFrame.from_records(
                [(col, self.data[col].nunique()) for col in self.data.columns],
                columns=['feature', 'num_unique']).sort_values(by='num_unique')

            print(unique_counts)

        def clean_label():
            log("Removing duplicate plans")
            plans = np.array(self.data[self.planName])
            print(plans[plans == 'MOLINA MEDICARE'])
            for dups in dup_names:
                replace = dups[0]
                print(replace)
                cnt = 0
                for i in range(len(plans)):
                    if plans[i] in dups:
                        plans[i] = replace
                        cnt += 1
                print('\t', str(cnt))
            print(len(set(plans)))
            self.data[self.planName] = plans

            unique_counts = pd.DataFrame.from_records(
                [(col, self.data[col].nunique()) for col in self.data.columns],
                columns=['feature', 'num_unique']).sort_values(by='num_unique')

            print(unique_counts)

        log("Starting cleaning data")
        plans = np.array(self.data[self.planName])
        print(sum(plans == 'MOLINA MEDICARE'))
        rm_na()
        rm_dup()
        clean_label()
        log("Finished cleaning data")

    def __data_transfer_Y(self):
        log("Start Transforming Y")
        Y = self.data[self.planName]
        plans = list(set(Y))

        planToCat = {
            p: i for p, i in zip(plans, range(len(plans)))
        }

        label = transfer(
            dictionary=planToCat, arr=Y
        )

        self.label = label
        print("self.label")
        print(self.label[:10])
        log("Finished transforming Y")

    def __data_transfer_X(self):
        log("Start Transforming X")
        X = self.data.loc[:, self.data.columns != self.planName]
        locations = X[self.location]
        uniqueLoc = set(locations)
        locToNum = {
            l: i for l, i in zip(uniqueLoc, range(len(uniqueLoc)))
        }
        location_state = transfer(locToNum, locations)
        X[self.location] = location_state
        self.X = X
        print("self.X")
        print(self.X[:10])
        log("Finished Transforming X")

    def __feature_engineer(self, letters):
        log("Start Feature Engineering")
        policyNum = list(self.X.pop(self.policy))

        def add_length():
            length = [len(policy) for policy in policyNum]
            self.X['length'] = length

        def letter_counter():
            A = 65
            name = A
            for i in range(26):
                cur_name = chr(name + i)
                cur_counter = [
                    # Count number of current letters in every policy number
                    cur_policy.count(cur_name) for cur_policy in policyNum
                ]
                self.X[cur_name] = cur_counter

        def location_numbers():
            def split(word):
                return list(word)

            def to_str(lst):
                return ''.join(map(str, lst))

            place = []
            numberAlone = []
            for i in range(len(policyNum)):
                cur_policy = policyNum[i]
                chars = [int(not char.isnumeric()) for char in split(cur_policy)]
                temp = to_str(chars)
                if temp != '':
                    place.append(int(temp))
                ## In case of ASLKFJ1234ALFJ12
                nums = []
                for j in range(len(chars)):
                    if not chars[j]:
                        nums.append(cur_policy[j])
                temp = to_str(nums)
                if temp != '':
                    nums = int(temp)
                else:
                    nums = 0
                numberAlone.append(nums)

            self.X['place'] = place
            self.X['number'] = numberAlone

        add_length()
        if letters:
            letter_counter()
        location_numbers()

        print("processed self.X")
        print(self.X[:10])
        log("Finished Feature Engineering")

    ############################## Public ##############################

    def head(self, num=5):
        self.data.head(num)

    def init(self, letters=True):
        log("Starting initializing dataset")
        self.__clean_data()
        self.__data_transfer_Y()
        self.__data_transfer_X()
        self.__feature_engineer(letters)
        log("Finished initializing dataset")

    def generate_train_test(self):
        self.X_train, self.X_valid, self.y_train, self.y_valid = train_test_split(
            self.X, self.label,
            test_size=0.2,
            random_state=random_state
        )
        msg = str(int(self.test_size * 100)) + "/" + str(int((1 - self.test_size) * 100)) + " split"
        log(msg + " Done")

    def get_train(self):
        return self.X_train, self.y_train

    def get_valid(self):
        return self.X_valid, self.y_valid

    ############################## Exploratory ##############################

    def value_count(self):
        print(self.data[self.planName].value_counts())

    def generate_all_graphs(self):
        self.location_state_graph()
        self.plan_distribution_graph()
        self.plan_length_distribution_graph()
        self.is_number_distribution_graph()

    def location_state_graph(self):
        g = sns.countplot(
            x=self.location,
            data=self.data,
            order=self.data[self.location].value_counts().index)

        title = "Location Distribution graph" + ".png"
        save_img(title, g, img_dir)
        return g

    def plan_distribution_graph(self, top=30):
        sns.set(rc={'figure.figsize': (15, 30)})
        g = sns.countplot(
            y=self.data[self.planName],
            order=self.data[self.planName].value_counts().index[:top]
        )
        title = "TOP " + str(top) + " Plan Distribution graph" + ".png"
        save_img(title, g, img_dir)
        return g

    def is_number_distribution_graph(self):
        def is_numeric_paranoid(obj):
            return obj.isnumeric()

        npPN = self.data[self.policy]
        returned = list(map(is_numeric_paranoid, npPN))

        sns.set_style('darkgrid')
        g = sns.countplot(returned)
        title = "Num vs Non-Num Distribution graph" + ".png"
        save_img(title, g, img_dir)
        return g

    def plan_length_distribution_graph(self):
        plt.clf()
        lens = list(map(len, self.data[self.policy]))

        plt.xlim(0, 20)
        sns.set_style('darkgrid')
        g = sns.histplot(lens)

        title = "Plan Length Distribution" + ".png"
        save_img(title, g, img_dir)
        return g
