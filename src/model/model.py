from sklearn import metrics
from sklearn.metrics import accuracy_score, balanced_accuracy_score, cohen_kappa_score
from sklearn.ensemble import RandomForestClassifier
from imblearn.metrics import classification_report_imbalanced
## Grid Search Boosting
from sklearn.experimental import enable_halving_search_cv  # noqa
from sklearn.model_selection import GridSearchCV
from sklearn.model_selection import HalvingGridSearchCV
from catboost import CatBoostClassifier, Pool
from ..script.data import Data
from ..utils.utils import log
from ..utils.constants import *


class Model:

    def __init__(self, random_state = random_state):
        self.random_state = random_state

    def eval(self, dataset):
        X_valid, y_valid = dataset.get_valid()
        pred = self.model.predict(X_valid)

        def basic_ACC():
            log("Basic ACC Report")
            print("ACC: ", sum(pred == y_valid) / len(pred))
            print("Balanced ACC: ", metrics.balanced_accuracy_score(y_valid, pred))

        def conf_matrix():
            log("Confusion Matrix")
            conf_matrix = metrics.confusion_matrix(y_valid, pred)
            print(conf_matrix)

        def imbalanced_report():
            log("Imbalanced Classification Report")
            print(classification_report_imbalanced(y_valid, pred))

        def cohen_kappa():
            log("Cohen's Kappa")
            print("Cohen's kappa:", cohen_kappa_score(y_valid, pred))

        basic_ACC()
        imbalanced_report()
        cohen_kappa()
        conf_matrix()

    def get_importance(self):
        features = list(self.X_train.columns)
        importances = list(self.model.feature_importances_)
        feature_importances = [(feature, round(importance, 2)) for feature, importance in zip(features, importances)]
        feature_importances = sorted(feature_importances, key=lambda x: x[1], reverse=True)
        print("\nList of feature importances in descending order:")
        [print("Feature: {:20} Importance: {}".format(*pair)) for pair in feature_importances]


class RandomForest(Model):
    model = RandomForestClassifier(
        n_estimators=200,
        criterion='entropy',
        max_features='auto',
        random_state=random_state,
        n_jobs=-1
    )

    def train(self, dataset):
        self.X_train, self.y_train = dataset.get_train()
        self.model.fit(self.X_train, self.y_train)

    def tune(self, param_grid):

        sh = HalvingGridSearchCV(
            estimator=self.model,
            param_grid=param_grid,
            cv=5,
            factor=2,
            min_resources=20).fit(self.X_train, self.y_train)

        log("Finished Tuning")
        print(sh.best_estimator_)
        print(sh.best_params_)
        self.model = sh.best_estimator_


class CatBoost(Model):
    model = CatBoostClassifier(
        iterations=epoch,
        learning_rate=learning_rate,
        task_type="GPU",
        devices='0:1',
        custom_metric=['MultiClass']
    )

    def train(self, dataset):
        self.X_train, self.y_train = dataset.get_train()
        X_valid, y_valid = dataset.get_valid()
        eval_set = Pool(X_valid, y_valid)
        self.model.fit(
            self.X_train,
            self.y_train,
            eval_set=eval_set,
            verbose=False
        )

