def log(msg):
    print("#" * 25, msg, "#" * 25, '\n')


def save_img(title, g, img_dir):
    g.figure.savefig(img_dir + title)
    log(title + " Saved")


flatten = lambda t: [item for sublist in t for item in sublist]
