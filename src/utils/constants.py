import os
from .utils import flatten

random_state = 327
learning_rate = 0.001
epoch = 1000
img_dir = "img"


def directory_find(atom, root='.'):
    for path, dirs, files in os.walk(root):
        if atom in dirs:
            return os.path.join(path, atom)


img_dir = directory_find(img_dir) + '/'

aetna = [
    'AETNA',
    'AETNA BETTER HEALTH',
    'AETNA CAP/ONLY MAP AETNA CAP TO INS',
    'AETNA MEDICARE'
]

bcbs = [
    'BCBS',
    'HIGHMARK BCBS DELAWARE BLUE CARD',
    'HIGHMARK BCBS LOCAL'
]

fhcp = [
    'FHCP MEDICARE',
    'FHCP POS and  HMO ANYTIME'
]

humana = [
    'HUMANA',
    'HUMANA GOLD PLUS (HMO)',
    'HUMANA MEDICAID',
    'HUMANA MEDICARE PPO'
]

keystone = [
    'KEYSTONE CAP',
    'KEYSTONE FIRST',
    'KEYSTONE HEALTH PLAN EAST'
]

molina = [
    'MOLINA MARKETPLACE',
    'MOLINA MEDICAID',
    'MOLINA MEDICARE'
]

sunshine = [
    'SUNSHINE ALLWELL',
    'SUNSHINE AMBETTER',
    'SUNSHINE MEDICAID'
]

superior = [
    'SUPERIOR ALLWELL',
    'SUPERIOR AMBETTER'
]

uhc = [
    'UHC',
    'UHC CARE IMPROVEMENT PLUS',
    'UHC COMMUNITY PLAN',
    'UHC DUAL COMPLETE',
    'UHC MEDICARE ADVANTAGE',
    'UHC Medicaid Kancare',
    'UHC STUDENT RESOURCES'
]

dup_names = [
    aetna,
    bcbs,
    fhcp,
    humana,
    keystone,
    molina,
    sunshine,
    superior,
    uhc
]

